# Copyright 2010 Ingmar Vanhassel
# Copyright 2012 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson

export_exlib_phases src_compile src_test src_install

SUMMARY="Mesa's OpenGL demos"
DESCRIPTION="glxdemo, glxgears, glxheads, glxinfo & eglinfo"
HOMEPAGE="https://www.mesa3d.org"
DOWNLOADS="https://mesa.freedesktop.org/archive/demos/${PV}/${PNV}.tar.bz2"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        media-libs/glew[>=1.5.4]
        x11-dri/glu
        x11-dri/mesa[>=9]
        x11-libs/libX11
        x11-libs/libXext
"

MESON_SRC_CONFIGURE_PARAMS+=(
    # This is based on the tools we installed before with the autotools
    # build. Feel free to adjust to your needs.
    -Dwith-glut=''
    -Degl=enabled
    -Dgles1=disabled
    -Dgles2=disabled
    -Dlibdrm=disabled
    -Dosmesa=disabled
    -Dwayland=disabled
    -Dx11=enabled
)

mesa-demos_src_compile() {
    eninja src/xdemos/glx{demo,gears,heads,info}
    eninja src/egl/opengl/eglinfo
}

mesa-demos_src_test() {
    # Avoid a lot of building stuff we don't install and from that only
    # glxgears is used in tests and we disable glut above which is needed.
    :
}

mesa-demos_src_install() {
    dobin src/xdemos/glx{demo,gears,heads,info}
    dobin src/egl/opengl/eglinfo

    edo cd "${MESON_SOURCE}"
    emagicdocs
}

