# Copyright 2013-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: QtMultimedia"
DESCRIPTION="Qt Multimedia is an essential module that provides a rich set of QML types
and C++ classes to handle multimedia content. It also provides necessary APIs to access
the camera and radio functionality."

LICENCES+=" GPL-2"
MYOPTIONS="
    alsa examples gstreamer openal pulseaudio
"

QT_MIN_VER=$(ever range -3)

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.26]
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXv
        x11-libs/qtbase:${SLOT}[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:${SLOT}[>=${QT_MIN_VER}]
        alsa? ( sys-sound/alsa-lib[>=1.0.10] )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-bad:1.0
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opengl]
        )
        openal? ( media-libs/openal )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.10] )
"

qtmultimedia_src_configure()  {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi
    EQMAKE_PARAMS+=( GST_VERSION=1.0 )

    local qmake_params=(
        --
        -no-directshow
        # "EVR in DirectShow and WMF"
        -no-evr
        -no-wmf
        $(qt_enable alsa)
        $(qt_enable gstreamer)
        $(qt_enable openal)
        $(qt_enable pulseaudio)
    )

    eqmake "${qmake_params[@]}"
}

