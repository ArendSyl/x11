# Copyright 2012 Joonas Sarajärvi <muep@iki.fi>
# Distributed under the terms of GNU General Public License v2

require github [ user=g-truc ] cmake

SUMMARY="Header-only C++ mathematics library"
HOMEPAGE+=" https://glm.g-truc.net"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-pr968.patch
    "${FILES}"/${PN}-pr1055.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=FALSE
    -DBUILD_STATIC_LIBS:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DGLM_TEST_ENABLE:BOOL=TRUE -DGLM_TEST_ENABLE:BOOL=FALSE' )

src_prepare() {
    cmake_src_prepare

    # Remove Werror and Weverything, only set with clang, currently breaks
    # (at least) with 15.* because of UTF8 in comments...
    edo sed -e '/add_compile_options(-Werror -Weverything)/d' \
        -i test/CMakeLists.txt
}
src_install() {
    cmake_src_install

    edo rm "${IMAGE}"/usr/$(exhost --target)/include/${PN}/detail/*.cpp \
           "${IMAGE}"/usr/$(exhost --target)/include/${PN}/CMakeLists.txt

    if option doc ; then
        insinto /usr/share/doc/${PNVR}
        doins "${CMAKE_SOURCE}"/doc/${PN}.pdf
        insinto /usr/share/doc/${PNVR}/html
        doins "${CMAKE_SOURCE}"/doc/api/*
    fi
}

